#!/bin/bash

# App starts at 0x8200
# Syntax: build-asm.sh <project path> <file>

# sh build.sh /Users/andrew/Dev/z80code/app/asm/helloworld main
# sh build.sh . main

FILE=$2
Z80BIOS=/Users/andrew/Dev/z80code/asm/bios/main.hex

# clean up
cd $1
rm -f *.adb *.rel *.bin *.ihx *.lst *.map *.noi *.sym *.lk *.byte *.rom *.hex *.byte *.def *.lis *.list *.lib *.dis *.ref *.txt

# build
echo "Building rom file"
/Users/andrew/Dev/Retro/zasm/zasm  --z80 -uv2wybz $1/$FILE.asm

if test -e "$1/$FILE.rom";
	then 
	
	# create bin
	echo "Convert ROM to BIN"
	mv $1/$FILE.rom $1/$FILE.bin
	
	# Convert to hex
	echo "Converting to HEX file"
	/Users/andrew/Dev/Retro/z88dk/bin/z88dk-appmake +hex --org 0x8200 -b $1/$FILE.bin -o $1/$FILE.hex

	if test -e "$1/$FILE.hex";
	then

		#emulate in Z80
		echo "Merging HEX files"
		/Users/andrew/Dev/Retro/mergehex/mergehex -m $Z80BIOS $1/$FILE.hex -o $1/emulate.hex
		
		if test -e "$1/emulate.hex";
		then
			echo "Building Emulation BIN file"
			/Users/andrew/Dev/Retro/hex2bin/hex2bin -p 00 -s 0 -l FFFF $1/emulate.hex
		else
			echo "ERROR: Building z80 Emulation binary failed!"
			echo "ERROR: Cannot find $Z80BIOS"
			echo "ERROR: Missing $1/emulate.hex file"
		fi

	else
		echo "ERROR: Converting binary to HEX failed!"
		echo "ERROR: Missing $1/$FILE.hex"
	fi

	
	
	# Convert to bytes
	echo "Creating BYTE file"
	/usr/bin/hexdump -v -e ' 1/1 "%02X" ' $1/$FILE.bin > $1/$FILE.byte	
	if test -e "$1/$FILE.byte";
	then
		echo "BYTE file successful"
	else
		echo "ERROR: Converting BYTE file failed!"
		echo "ERROR: Missing $FILE.byte"
	fi
	
	# Convert to binary text
	echo "Creating binary TXT file"
	/usr/bin/xxd -g1 -u $1/$FILE.bin > $1/$FILE.txt
	if test -e "$1/$FILE.txt";
	then
		echo "TXT file successful"
	else
		echo "ERROR: Converting TXT file failed!"
		echo "ERROR: Missing $FILE.txt"
	fi
	
	# Disassemble
	#/Users/andrew/Dev/Retro/asz80/bin/dz80 /M=33280 /S=33280 /L $FILE.bin $FILE.dis
	#z80dismblr-macos --addbytes --bin 0x0000 $1/$FILE.bin --codelabel $APPLOC --out $1/$FILE.dis
	
else
		echo "ERROR: Build failed!"
		echo "ERROR: Missing $1/$FILE.rom"
fi

# tidy
	rm -f *.rel *.map *.noi *.sym *.lk *.def *.lis *.lib
# *.lis
