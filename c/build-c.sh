#!/bin/bash

# App starts at 0x8200
# Syntax: build-c.sh <project path> <file>

# sh build.sh /Users/andrew/Dev/z80code/c/app/helloworld main
# sh build.sh . main

FILE=$2

SDCCPATH=/opt/homebrew/opt/sdcc/bin
SDCC=$SDCCPATH/sdcc
Z80LIB=/Users/andrew/Dev/z80code/app/libraries
Z80BIOS=/Users/andrew/Dev/z80code/asm/bios/main.hex
ZSDCC=/Users/andrew/Dev/Retro/z88dk/bin/ZSDCC

APPLOC=0x8200
CODELOC=0x8240
DATALOC=0xC000
XRAMLOC=0xE000
STACKLOC=0xFFFF

PUTCHAR=putcharBios
GETCHAR=getcharBios
CRT=z80crt0

# clean up
cd $1
rm -f *.adb *.asm *.rel *.bin *.ihx *.lst *.map *.noi *.sym *.lk *.byte *.rom *.hex *.byte *.def *.lis *.list *.lib *.dis *.ref

# getchar.rel:
	echo -e "\033[1m\033[92mCompiling GETCHAR code ...\033[0m"
	$SDCCPATH/sdasz80 -o $Z80LIB/$GETCHAR.s

# putchar.rel:
	echo -e "\033[1m\033[92mCompiling PUTCHAR code ...\033[0m"
	$SDCCPATH/sdasz80 -o $Z80LIB/$PUTCHAR.s

# crt0.rel:
	echo -e "\033[1m\033[92mCompiling CRT0 code ...\033[0m"
	$SDCCPATH/sdasz80 -o $Z80LIB/$CRT.s
	
# sdcc: $SDCCPATH/sdcc
	echo -e "\033[1m\033[92mCompiling C code ...\033[0m"
	$SDCC -mz80 --std-sdcc99 --allow-unsafe-read --no-c-code-in-asm --opt-code-speed --less-pedantic \
		--code-loc $CODELOC \
		--data-loc $DATALOC \
		--no-std-crt0 $Z80LIB/$CRT.rel $Z80LIB/$PUTCHAR.rel $Z80LIB/$GETCHAR.rel \
		 --disable-warning 85 --disable-warning 196 --max-allocs-per-node 100000 \
		$1/$FILE.c

# pack:
	if test -e "$1/$FILE.ihx";
	then 
		# pack:
		echo -e "\033[1m\033[92mPacking HEX file ...\033[0m"
		$SDCCPATH/packihx $1/$FILE.ihx>$1/$FILE.hex
		
		if test -e "$1/$FILE.hex";
		then
		
			#bin:
			echo -e "\033[1m\033[92mCreating BIN file ...\033[0m"
			/Users/andrew/Dev/Retro/hex2bin/hex2bin -p 00 -s 0 -l FFFF $1/$FILE.hex
			
			#dis
			echo -e "\033[1m\033[92mDisassembling BIN file ...\033[0m"
			#/Users/andrew/Dev/Retro/asz80/bin/dz80 /L /S=33280 main.bin main.dis
			/Users/andrew/Dev/Retro/asz80/bin/dz80 /L /S=33280 $1/$FILE.bin $1/$FILE.dis
			
			#emulate in Z80
			echo -e "\033[1m\033[92mBuilding z80 Emulation binary ...\033[0m"
			/Users/andrew/Dev/Retro/mergehex/mergehex -m $Z80BIOS $1/$FILE.hex -o $1/emulate.hex
			
			if test -e "$1/emulate.hex";
			then
				/Users/andrew/Dev/Retro/hex2bin/hex2bin -p 00 -s 0 -l FFFF $1/emulate.hex
			else
				echo -e "ERROR: Building z80 Emulation binary failed!"
				echo -e "ERROR: Cannot find $Z80BIOS"
				echo -e "ERROR: Missing $1/emulate.hex file"
			fi
			
		else
				echo -e "ERROR: Packing HEX file failed!"
				echo -e "ERROR: Missing $1/$FILE.hex"
		fi
		
	fi

# tidy
	rm -f *.rel  *.map *.noi *.sym *.lk *.def  *.lib *.lst *.lis


	
	
	